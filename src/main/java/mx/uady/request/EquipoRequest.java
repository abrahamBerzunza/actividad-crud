package mx.uady.request;

public class EquipoRequest {

    private Integer id;
    private String modelo;
    
    public EquipoRequest() {}

    public Integer getId() {
        return id;
    }

	public String getModelo() {
		return modelo;
	}
}