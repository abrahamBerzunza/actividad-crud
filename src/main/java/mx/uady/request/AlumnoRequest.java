package mx.uady.request;

public class AlumnoRequest {

  private Integer id;
  private String nombre;
  private String licenciatura;

  public AlumnoRequest() {}

  /**
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }

  /**
   * @return the licenciatura
   */
  public String getLicenciatura() {
    return licenciatura;
  }

  /**
   * @param nombre the nombre to set
   */
  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  /**
   * @param licenciatura the licenciatura to set
   */
  public void setLicenciatura(String licenciatura) {
    this.licenciatura = licenciatura;
  }

  /**
   * @return the id
   */
  public Integer getId() {
    return id;
  }

  /**
   * @param id the id to set
   */
  public void setId(Integer id) {
    this.id = id;
  }
}