package mx.uady.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "profesores")
public class Profesor {

    @Id
    private Integer id;

    @Column
    private String nombre;

    @Column
    private Integer horas;

    @OneToMany(mappedBy = "profesor")
    private List<Tutoria> tutorias;

    public Profesor() {
    }

    public Profesor(Integer id, String nombre, Integer horas) {
        this.id = id;
        this.nombre = nombre;
        this.horas = horas;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getHoras() {
        return horas;
    }

    public void setHoras(Integer horas) {
        this.horas = horas;
    }

    public Profesor id(Integer id) {
        this.id = id;
        return this;
    }

    public Profesor nombre(String nombre) {
        this.nombre = nombre;
        return this;
    }

    @Override
    public String toString() {
        return "{" + " id='" + getId() + "'" + ", nombre='" + getNombre() + "'" + ", horas='" + getHoras() + "'" + "}";
    }

}