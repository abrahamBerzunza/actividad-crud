package mx.uady.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import mx.uady.model.Equipo;

@Repository
public interface EquipoRepository extends CrudRepository<Equipo, Integer> {

}