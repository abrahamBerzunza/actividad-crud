package mx.uady.repository;

import java.util.stream.Stream;

import javax.persistence.Id;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import mx.uady.model.Tutoria;;

@Repository
public interface TutoriaRepository extends CrudRepository<Tutoria, Integer> {
}