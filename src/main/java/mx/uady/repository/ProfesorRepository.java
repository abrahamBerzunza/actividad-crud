package mx.uady.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import mx.uady.model.Profesor;

@Repository
public interface ProfesorRepository extends CrudRepository<Profesor, Integer> {

}