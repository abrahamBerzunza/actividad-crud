package mx.uady.resource;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import mx.uady.model.Alumno;
import mx.uady.model.Equipo;
import mx.uady.repository.AlumnoRepository;
import mx.uady.repository.EquipoRepository;
import mx.uady.request.AlumnoRequest;

@RestController
public class AlumnoResource {

    @Autowired
    private AlumnoRepository alumnoRepository;

    @Autowired EquipoRepository equipoRepository;

    @GetMapping("/alumnos")
    public List<Alumno> alumnos() {
        List<Alumno> alumnos = new LinkedList<>();
        alumnoRepository.findAll().iterator().forEachRemaining(alumnos::add);
        return alumnos;
    }

    @PostMapping("/alumno")
    public ResponseEntity<Alumno> alumno(@RequestBody AlumnoRequest request) throws URISyntaxException {
        Alumno alumno = new Alumno();
        alumno.setId(request.getId());
        alumno.setNombre(request.getNombre());
        alumno.setLicenciatura(request.getLicenciatura());
        alumnoRepository.save(alumno);
        URI location = new URI("/alumnos");
        ResponseEntity<Alumno> response = ResponseEntity.created(location).body(alumno);
        return response;
    }

    @DeleteMapping("alumno/{id}")
    public ResponseEntity<Alumno> alumno(@PathVariable Integer id) {
        alumnoRepository.deleteById(id);
        return ResponseEntity.ok().build();
    }

    @PutMapping("alumno")
    public ResponseEntity<Alumno> actualizarAlumno(@RequestBody AlumnoRequest request) {
        Optional<Alumno> alumnoOptional = alumnoRepository.findById(request.getId());

        if (!alumnoOptional.isPresent()) {
            return ResponseEntity.notFound().build();
        }

        Alumno alumno = new Alumno();
        alumno.setId(request.getId());
        alumno.setNombre(request.getNombre());
        alumno.setLicenciatura(request.getLicenciatura());
        alumnoRepository.save(alumno);
        
        return ResponseEntity.ok().build();
    }

    @PostMapping("/alumnos/{id}/agregarEquipo/{idEquipo}")
    public ResponseEntity<Alumno> agregarEquipo(@PathVariable Integer id, @PathVariable Integer idEquipo) {
        Optional<Alumno> alumnoOptional = alumnoRepository.findById(id);
        Optional<Equipo> equipoOptional = equipoRepository.findById(idEquipo);

        if (!alumnoOptional.isPresent() || !equipoOptional.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        
        Alumno alumno = alumnoOptional.get();
        alumno.setEquipoId(equipoOptional.get());
        alumnoRepository.save(alumno);

        return ResponseEntity.ok().build();
    }

}