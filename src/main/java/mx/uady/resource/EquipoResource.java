package mx.uady.resource;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import mx.uady.model.Equipo;
import mx.uady.repository.EquipoRepository;
import mx.uady.request.EquipoRequest;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.PathVariable;

@RestController
public class EquipoResource {

    @Autowired
    private EquipoRepository equipoRepository;

    @GetMapping("/equipos")
    public List<Equipo> equipos() {
        List<Equipo> equipos = new LinkedList<>();
        equipoRepository.findAll().iterator().forEachRemaining(equipos::add);
        return equipos;
    }

    @PostMapping("equipo")
    public ResponseEntity<Equipo> equipo(@RequestBody EquipoRequest request) throws URISyntaxException {
        Equipo equipo = new Equipo();
        equipo.setId(request.getId());
        equipo.setModelo(request.getModelo());

        equipoRepository.save(equipo);
        URI location = new URI("/equipos");
        ResponseEntity<Equipo> response = ResponseEntity.created(location).body(equipo);

        return response;
    }

    @PutMapping("equipo")
    public ResponseEntity<Equipo> actualizarEquipo(@RequestBody EquipoRequest request) {
        Optional<Equipo> equipoOptional = equipoRepository.findById(request.getId());

        if (!equipoOptional.isPresent()) {
            return ResponseEntity.notFound().build();
        }

        Equipo equipo = new Equipo();
        equipo.setId(request.getId());
        equipo.setModelo(request.getModelo());
        equipoRepository.save(equipo);
        
        return ResponseEntity.ok().build();
    }
    
}