package mx.uady.resource;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import javax.persistence.Id;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import mx.uady.model.Tutoria;
import mx.uady.repository.TutoriaRepository;

public class TutoriaResource {

    @Autowired
    private TutoriaRepository tutoriaRepository;

    @GetMapping("/tutorias")
    public List<Tutoria> getTutorias() {
        List<Tutoria> tutorias = new LinkedList<Tutoria>();
        tutoriaRepository.findAll().iterator().forEachRemaining(tutorias::add);
        return tutorias;
    }

    @GetMapping("/tutorias/{id}")
    public ResponseEntity<Tutoria> getTutoria(@PathVariable Integer id) {
        Optional<Tutoria> tutoria = tutoriaRepository.findById(id);
        return ResponseEntity.ok().body(tutoria.get());
    }

    @PostMapping("/tutorias")
    public ResponseEntity<Tutoria> saveTutoria(@RequestBody Tutoria request) throws URISyntaxException {
        Tutoria tutoria = tutoriaRepository.save(request);
        URI location = new URI("/tutorias/" + tutoria.getId());
        ResponseEntity<Tutoria> response = ResponseEntity.created(location).body(tutoria);
        return response;
    }

    @DeleteMapping("/tutorias/{id}")
    public ResponseEntity<Void> eliminarTutoria(@PathVariable Integer id) {
        Optional<Tutoria> tutoria = tutoriaRepository.findById(id);
        tutoriaRepository.delete(tutoria.get());

        return ResponseEntity.ok().build();
    }

    @PutMapping("/tutorias/")
    public ResponseEntity<Tutoria> editarAlumno(@RequestBody Tutoria request) {
        Tutoria tutoria = tutoriaRepository.save(request);
        return ResponseEntity.ok().body(tutoria);
    } 
}