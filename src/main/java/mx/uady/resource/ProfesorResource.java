package mx.uady.resource;

import java.net.URI;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import mx.uady.model.Profesor;
import mx.uady.repository.ProfesorRepository;

@RestController
public class ProfesorResource {

    @Autowired
    private ProfesorRepository profesorRepository;

    @GetMapping("/profesores")
    public List<Profesor> profesores() {
        List<Profesor> profesores = new LinkedList<>();
        profesorRepository.findAll().iterator().forEachRemaining(profesores::add);
        return profesores;
    }

    @PostMapping("/profesores")
    public ResponseEntity<Object> createProfesor(@RequestBody Profesor profesor) {
        Profesor savedProfesor = profesorRepository.save(profesor);

        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(savedProfesor.getId()).toUri();

        return ResponseEntity.created(location).build();

    }

    @PutMapping("/profesores/{id}")
    public ResponseEntity<Object> updateProfesor(@RequestBody Profesor profesor, @PathVariable int id) {

        Optional<Profesor> profesorOptional = profesorRepository.findById(id);

        if (!profesorOptional.isPresent())
            return ResponseEntity.notFound().build();

        profesor.setId(id);

        profesorRepository.save(profesor);

        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/profesores/{id}")
    public void deleteProfesor(@PathVariable int id) {
        profesorRepository.deleteById(id);
    }
}